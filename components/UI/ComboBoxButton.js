import React, {useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Colors  from '../../constants/Colors';

const dummyArr = [
    {
        title: 'Body building',
        value: 'Body building',
        checked: false,
    },
    {
        title: 'Mental Health',
        value: 'Mental Health',
        checked: false,
    },
    
    {
        title: 'Covid-19',
        value: 'Covid-19',
        checked: false,
    },
    {
        title: 'Low Budget',
        value: 'low_budget',
        checked: false,
    },
    {
        title: 'Vegun Diet',
        value: 'Vegun Diet',
        checked: false,
    },
    
     {
        title: 'Sex Education',
        value: 'Sex Education',
        checked: false,
    },
    {
        title: 'Breast Cancer Awareness',
        value: 'Breast Cancer Awareness',
        checked: false,
    },
    {
        title: 'Anexity',
        value: 'Anexity',
        checked: false,
    },
    
];

const ComboBoxButton = ({data = dummyArr, callback, 
    checked_color = "#DD502C"    }) =>{

    const [dataArr, setDataArr] = useState(data);

    const onCheck = (item, idx) => {
        return () => {
            const arr = [...dataArr];
            arr[idx].checked = !arr[idx].checked;
            setDataArr(arr);
            const result = arr.filter(arr => arr.checked === true);
            // console.log('result----->', result);
            return callback(result);
        };
    };
    return (
    <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
        {
          dataArr.map((x, y) => {
             return (
                 <TouchableOpacity onPress={onCheck(x, y)} key={y}
                      style={{
                          height: 30,
                          backgroundColor: x.checked ? checked_color : '#f0f8ff',
                          paddingHorizontal: 8,
                          alignItems: 'center',
                          justifyContent: 'center',
                          borderRadius:25,
                          marginBottom: 5,
                          marginRight: 5,
                          borderWidth: 1
                      }}>
                     <Text style={{color:x.checked ? '#f3f3f3' : '#000'}}>{x.title}</Text>

                 </TouchableOpacity>
             );
            })
        }

    </View>
    );
}
export default ComboBoxButton;