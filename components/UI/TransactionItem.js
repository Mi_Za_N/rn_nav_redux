import React from 'react';
import { View, Text, StyleSheet,Image } from 'react-native';

const TransactionItem = ({data}) => {
    const url ="https://res.cloudinary.com/dsop4plsb/image/upload/v1640870031/wpgxhndewx0uxz5xs95a.jpg";
    return (
        <View style={{
            // height: 95,
            width: 295,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
            marginHorizontal: 10,
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.3,
            shadowRadius: 5,
            borderRadius: 5,
            elevation: 3,
            backgroundColor: 'white',
            paddingHorizontal: 20,
            paddingVertical: 25
        }}>
            <View style={{
                width: 40,
                height: 40,
                borderRadius: 20,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#f5a623'
            }}>
                <Image source={{ uri: url}}
                       style={styles.profileImg}/>
            </View>
            <View style={{flex: 1, paddingHorizontal: 20}}>
                <Text style={{fontSize: 12, fontWeight: 'bold', color: '#dc2929'}}>29 Dec 2022</Text>
                <Text style={{fontSize: 14, fontWeight: 'bold', color: '#413d4a'}}>Event Title</Text>
                <Text style={{fontSize: 14, fontWeight: 'bold', color: '#adb3c8', marginTop: 4}}>Lorum ipsum dollar site emmet</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
 profileImg: {
      width: 65, 
      height: 65, 
      resizeMode: 'contain',
      borderRadius: 80,
    },
});

export default TransactionItem;