import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Toast from "react-native-toast-message";

// import TopBarNavigation from "./Navigators/TopBarNavigation";
import BottomBarNavigation from './Navigators/BottomTabNavigation';
// import StackNavigator from './Navigators/StackNavigation';
// import DrawerNavigation from './Navigators/DrawerNavigation';
// import RouteParamsNavigation from './Navigators/RouteParamsNavigator';

import { Provider } from "react-redux";
import store from "./store/store";

export default function App() {
    return (
        <Provider store={store}>
        <SafeAreaProvider>
            <BottomBarNavigation />
            <Toast ref={(ref) => Toast.setRef(ref)} />
        </SafeAreaProvider>
        </Provider>
    );
}