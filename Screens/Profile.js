import React, { useEffect } from 'react';
import { View, Text, StyleSheet, Button,ScrollView } from "react-native";
import {logout } from "../store/actions/userActions";
import { useDispatch, useSelector } from "react-redux";


const Profile = (props) => {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;

    useEffect(() => {
    if (!userInfo) {
      props.navigation.navigate("Login");
    }
  }, [userInfo]);

 const dispath = useDispatch();
 const logoutHandler = () => {
    dispath(logout());
    props.navigation.navigate("Login");
  };

  return (
    <ScrollView contentContainerStyle={styles.subContainer}>
        <Text style={{ fontSize: 30 }}>
            {userInfo ? userInfo.name : "" }
        </Text>
        <View style={{ marginTop: 20 }}>
            <Text style={{ margin: 10 }}>
                Email: {userInfo ? userInfo.email : ""}
            </Text>
            <Text style={{ margin: 10 }}>
                Phone: {userInfo ? userInfo.phone : ""}
            </Text>
        </View>
        <View style={{ marginTop: 80 }}>
            <Button title={"Sign Out"} 
            onPress={logoutHandler}/>
        </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#48d969"
  },
  text: {
    fontSize: 20,
    color: "#ffffff",
    fontWeight: "800",
  },
});
export default Profile